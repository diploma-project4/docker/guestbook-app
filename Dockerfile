FROM php:8.3.3-apache
RUN apt-get update && apt-get -y install zip unzip
RUN pear channel-discover pear.nrk.io && pear install nrk/Predis
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer
WORKDIR /var/www/html
COPY . .

RUN sed -i 's#ErrorLog /proc/self/fd/2#ErrorLog "|$/bin/cat 1>\&2"#' /etc/apache2/apache2.conf
RUN sed -i 's#CustomLog /proc/self/fd/1 combined#CustomLog "|/bin/cat" combined#' /etc/apache2/apache2.conf
RUN composer install

EXPOSE 80
